#ifndef APPLICATION_H_
#define APPLICATION_H_

#include "hardware/GPIO.h"
#include "hardware/Uart.h"
#include "periphery/Battery.h"
#include "periphery/BtDriver.h"
#include "periphery/Engine.h"
#include "periphery/LedDriver.h"
#include "periphery/SensorDriver.h"

extern GPIO GPIO_A;
extern GPIO GPIO_B;
extern GPIO GPIO_C;
extern GPIO GPIO_D;
extern GPIO GPIO_E;
extern GPIO GPIO_F;

/* UART interface */
extern Uart Serial1;

/* Periphery */
extern Battery battery;
extern BtDriver btDriver;
extern Engine leftEngine;
extern Engine rightEngine;
extern LedDriver ledDriver;
extern SensorDriver sensorDriver;

#endif /* APPLICATION_H_ */