#include "Robot.h"

#include "Setup.h"
#include "std/String.h"
#include "time/delays.h"

Robot::Robot()
{
	
}

Robot::~Robot()
{
	
}

void Robot::setup()
{
	btDriver.setup();
	
	uint32_t baudrate = 9600;
	Serial1.begin(baudrate);	
}

void Robot::update()
{
	if(battery.status() == BATTERY_OK)
	{
		//healthCheck();
		//handleBt();
		doLinefollower();
	}
	else
	{
		leftEngine.stop();
		rightEngine.stop();
		ledDriver.turnOffAll();
		while(battery.status() == BATTERY_LOW)
		{
			battery.blinkLed();
		}
	}	
}

void Robot::healthCheck()
{
	ledDriver.turnOnLed7();
	delay100ms();
	ledDriver.turnOffLed7();
	delay100ms();
}

void Robot::doLinefollower()
{
	//printSensorMeasurements();
	
	uint16_t threshold = 400;
	bool sensorState[] = { 
		sensorDriver.sensor0() > threshold,
		sensorDriver.sensor1() > threshold,
		sensorDriver.sensor2() > threshold,
		sensorDriver.sensor3() > threshold,
		sensorDriver.sensor4() > threshold,
		sensorDriver.sensor5() > threshold,
		sensorDriver.sensor6() > threshold,
		sensorDriver.sensor7() > threshold 
	};
	//printSensorState(sensorState);
	showSensorStateOnLeds(sensorState);
	
	uint8_t sensorCountPerSide = 4;
	uint8_t maxSpeed = 150;
	uint8_t minSpeed = 0;
	uint8_t delta = maxSpeed / sensorCountPerSide;
	
	leftEngine.forward();
	rightEngine.forward();
	
	uint8_t rightWeight = sensorState[0] * 4 + sensorState[1] * 3 + sensorState[2] * 2 + sensorState[3] * 1;
	uint8_t leftWeight = sensorState[7] * 4 + sensorState[6] * 3 + sensorState[5] * 2 + sensorState[4] * 1;
	
	if( (leftWeight == 1 && rightWeight == 0) || (leftWeight == 0 && rightWeight == 1) )
	{
		rightEngine.setSpeed(maxSpeed);
		leftEngine.setSpeed(maxSpeed);
	}
	else if(rightWeight > 0 || leftWeight > 0)
	{
		rightWeight = (rightWeight > leftWeight ? rightWeight : 0);
		leftWeight = (leftWeight > rightWeight ? leftWeight : 0);
		
		rightWeight = (rightWeight > sensorCountPerSide ? 4 : rightWeight);
		leftWeight = (leftWeight > sensorCountPerSide ? 4 : leftWeight);
		
		rightEngine.setSpeed(maxSpeed - rightWeight * delta);
		leftEngine.setSpeed(maxSpeed - leftWeight * delta);
	}
}

void Robot::showSensorStateOnLeds(bool* sensorState)
{
	uint8_t ledState =
		(sensorState[7] << 7) |
		(sensorState[6] << 6) |
		(sensorState[5] << 5) |
		(sensorState[4] << 4) |
		(sensorState[3] << 3) |
		(sensorState[2] << 2) |
		(sensorState[1] << 1) |
		(sensorState[0] << 0);
	ledDriver.turnOffAll();
	ledDriver.turnOnLed(ledState);	
}

void Robot::printSensorMeasurements()
{
	Serial1.println(
		String(sensorDriver.sensor7()) + String(" ") +
		String(sensorDriver.sensor6()) + String(" ") +
		String(sensorDriver.sensor5()) + String(" ") +
		String(sensorDriver.sensor4()) + String(" ") +
		String(sensorDriver.sensor3()) + String(" ") +
		String(sensorDriver.sensor2()) + String(" ") +
		String(sensorDriver.sensor1()) + String(" ") +
		String(sensorDriver.sensor0()) + String(" ")
	);	
}

void Robot::printSensorState(bool* sensorState)
{
	Serial1.println(
		String(sensorState[7]) + String(" ") +
		String(sensorState[6]) + String(" ") +
		String(sensorState[5]) + String(" ") +
		String(sensorState[4]) + String(" ") +
		String(sensorState[3]) + String(" ") +
		String(sensorState[2]) + String(" ") +
		String(sensorState[1]) + String(" ") +
		String(sensorState[0]) + String(" ")
	);	
}

void Robot::handleBt()
{
	uint8_t commandLength = 5;
	if(btDriver.available() >= commandLength)
	{
		uint8_t command[commandLength];
		uint8_t read = btDriver.read(command, commandLength);

		if(read == commandLength)
		{
			if(command[1] == 100)
			{
				handleDirectionCommand(leftEngine, command[2]);
				handleDirectionCommand(rightEngine, command[3]);
			}
			else if(command[1] == 200)
			{
				uint8_t speedLeft = command[2];
				uint8_t speedRight = command[3];
				leftEngine.setSpeed(speedLeft);
				rightEngine.setSpeed(speedRight);
			}
		}
	}
}

void Robot::handleDirectionCommand(Engine& engine, uint8_t direction)
{
	switch(direction)
	{
		case 0:
		{
			engine.setSpeed(0);
			break;
		}
		case 1:
		{
			engine.backward();
			break;
		}
		case 2:
		{
			engine.forward();
			break;
		}
	}
}
