#include "GPIO.h"

#include <avr/io.h>

GPIO::GPIO(volatile uint8_t* ddrArg, volatile uint8_t* portArg, volatile uint8_t* pinArg) : ddr(ddrArg), port(portArg), pin(pinArg)
{
	
}

GPIO::~GPIO()
{
	
}

void GPIO::setAsOutput(uint8_t pins)
{
	*ddr = *ddr | pins;
}

void GPIO::setAsInput(uint8_t pins)
{
	*ddr = *ddr & ~pins;
}

void GPIO::setPins(uint8_t pins)
{
	*port |= pins;
}

void GPIO::clearPins(uint8_t pins)
{
	*port = *port & ~pins;
}
	
uint8_t GPIO::read()
{
	return *pin;
}	
	
uint8_t GPIO::readPin(uint8_t singlePin)
{
	return ( (*pin) & (1 << singlePin) ) > 0;
}