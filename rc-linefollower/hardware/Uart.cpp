#include "Uart.h"

#include "Setup.h"
#include "std/String.h"

#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdlib.h>

Uart::Uart() : writeIndex(0), readIndex(0), bufSize(64), timeout(DEFAULT_TIMEOUT)
{
	buffer = (uint8_t *) malloc(sizeof(uint8_t) * bufSize);
}

Uart::~Uart()
{
	free(buffer);
}

void Uart::setup()
{
	
}

void Uart::update()
{
	if(writeIndex == readIndex)
	{
		writeIndex = readIndex = 0;
	}
	writeIndex %= bufSize;
	if(!writeIndex)
	{
		readIndex = 0;
	}
	buffer[writeIndex] = UDR;
	++writeIndex;
}

void Uart::begin(uint32_t baudrate)
{
	/* Set baud rate */
	uint16_t baud = (F_CPU / (16 * baudrate) - 1);
	UBRRH = (uint8_t)(baud>>8);
	UBRRL = (uint8_t)baud;
	/* Enable receiver and transmitter */
	UCSRB = (1 << RXCIE) | (1 << RXEN) | (1 << TXEN);
	/* Set frame format: 8data, 1stop bit */
	UCSRC =  ( 1 << URSEL ) | (1 << UCSZ0) | (1 << UCSZ1);	
}

uint8_t Uart::available()
{
	return writeIndex - readIndex;
}

void Uart::print(const char* s)
{
	while(*s) 
	{
		write(*s);
		++s;
	}
}

void Uart::write(const char c)
{
	/* Wait for empty transmit buffer */
	while ( !( UCSRA & (1 << UDRE)) );;
	/* Put data into buffer, sends the data */
	UDR = c;	
}

void Uart::println(const char* s)
{
	print(s);
	print("\r\n");
}

void Uart::println(const String& str)
{
	print(str.data());
	print("\r\n");
}

int16_t Uart::read()
{
	if(available())
	{
		readIndex %= bufSize;
		int16_t c = buffer[readIndex];
		++readIndex;
		return c;
	}
	return -1;	
}

int16_t Uart::readBytes(unsigned char* buf, uint8_t length)
{
	resetTimeout();
	uint8_t charCounter = 0;
	for(; charCounter < length && timeLeft > 0; ++charCounter) 
	{
		int16_t c;
		do 
		{
			c = read();
		} while(c < int16_t(0) && timeLeft > 0);
		
		if( timeLeft < 1 )
		{
			return charCounter;
		}
		
		buf[charCounter] = c;
	}
	return charCounter;
}

int16_t Uart::readBytesUntil(unsigned char character, unsigned char* buf, uint8_t length)
{
	resetTimeout();
	uint8_t charCounter = 0;
	for(; charCounter < length && timeLeft > 0; ++charCounter) 
	{
		int16_t c;
		do 
		{
			c = read();
		} while(c < 0 && timeLeft > 0);
		
		if( timeLeft < 1 )
		{
			return charCounter;
		}
		
		buf[charCounter] = c;
		if(c == character)
		{
			return charCounter + 1;
		}
	}
	return charCounter;
}

void Uart::resetTimeout()
{
	timeLeft = timeout;
}

void Uart::timeUp()
{
	if(timeLeft > 0)
	{
		--timeLeft;
	}
}

void Uart::setTimeout(uint32_t timeout)
{
	this->timeout = timeout;
}

ISR(USART_RXC_vect)
{
	Serial1.update();
}
