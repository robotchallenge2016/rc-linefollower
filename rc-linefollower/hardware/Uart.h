#ifndef __UART_H__
#define __UART_H__

#include <inttypes.h>

#include "time/Timeout.h"

class String;

class Uart : public Timeout
{
private:
	uint8_t *buffer;
	uint8_t writeIndex;
	uint8_t readIndex;
	uint8_t bufSize;
	uint32_t timeLeft;
	uint32_t timeout;
	
public:
	Uart();
	~Uart();
	
	void setup();
	void update();
	
	void begin(uint32_t baudrate);
	
	uint8_t available();
	
	void print(const char* s);
	void write(const char c);
	void println(const char* s);
	void println(const String& str);
	
	int16_t read();
	int16_t readBytes(uint8_t* buf, uint8_t length);
	int16_t readBytesUntil(uint8_t character, uint8_t* buf, uint8_t length);
	
	void resetTimeout();
	void timeUp();
	void setTimeout(uint32_t timeout);
	
protected:
private:
	Uart( const Uart &c );
	Uart& operator=( const Uart &c );

};

#endif //__UART_H__
