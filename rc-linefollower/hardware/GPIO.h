#ifndef __GPIO_H__
#define __GPIO_H__

#include <inttypes.h>

class GPIO
{
private:
	volatile uint8_t* ddr;
	volatile uint8_t* port;
	volatile uint8_t* pin;

public:
	GPIO(volatile uint8_t* ddrArg, volatile uint8_t* portArg, volatile uint8_t* pinArg);
	~GPIO();
	
	void setAsOutput(uint8_t pins);
	void setAsInput(uint8_t pins);
	
	void setPins(uint8_t pins);
	void clearPins(uint8_t pins);
	
	uint8_t read();
	uint8_t readPin(uint8_t pin);
};

#endif //__GPIO_H__
