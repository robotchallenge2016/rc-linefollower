#include <avr/io.h>
#include <avr/interrupt.h>

#include "Robot.h"
#include "Setup.h"

Robot robot;

void timer1ms()
{
	TCCR0 = (1 << WGM01) | (1 << CS01) | (1 << CS00);
	TIMSK |= (1 << OCIE0);
	OCR0 = 249;
}

void setup()
{
	timer1ms();
	robot.setup();
	sei();
}

void loop()
{
	robot.update();
}

int main(void)
{	
	setup();
    while (1)
    {
		loop();
    }
}

ISR(TIMER0_COMP_vect)
{
	Serial1.timeUp();
}