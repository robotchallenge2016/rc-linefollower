#include "String.h"

#include "Setup.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

String::String() : s(NULL), length(0)
{
	
}

//String::String(int8_t number)
//{
	//uint8_t length = 15;
	//s = (char*) malloc(sizeof(char) * (charCount + 1));
	//if(s != NULL) 
	//{
		//Serial.println("Allocated");
	//}
	//else
	//{
		//Serial.println("Not allocated");
	//}
	//uint8_t count = sprintf(s, "%d", number);
	//if(count > 0)
	//{
		//Serial.println("Success");
	//}
	//else
	//{
		//Serial.println("Fail");
	//}	
//}

String::String(uint8_t number) 
{
	this->length = 15;
	s = (char*) malloc(sizeof(char) * (length + 1));
	sprintf(s, "%u", number);
}

String::String(int16_t number)
{
	this->length = 15;
	s = (char*) malloc(sizeof(char) * (length + 1));
	sprintf(s, "%d", number);
}

String::String(uint16_t number)
{
	this->length = 15;
	s = (char*) malloc(sizeof(char) * (length + 1));
	sprintf(s, "%u", number);
}

String::String(const char* ss) : s(NULL)
{
	this->length = strlen(ss);
	s = (char*) malloc(sizeof(char) * (length + 1));
	strcpy(s, ss);
}

String::String(const String &ss) : s(NULL)
{
	s = (char*) malloc(sizeof(char) * (ss.length + 1));
	strcpy(s, ss.data());
	this->length = ss.length;
}

String::~String()
{
	free(s);
}

String& String::operator=(const String& o)
{
	if(s != NULL)
	{
		free(s);
	}
	s = (char*) malloc(sizeof(char) * (o.length + 1));
	strcpy(s, o.s);
	length = o.length;
	
	return *this;
}

String operator+(String &s1, String &s2)
{
	char *s = (char*) malloc(sizeof(char) * (s1.size() + s2.size() + 1));
	strcpy(s, s1.data());
	strcat(s, s2.data());
	
	String str(s);
	free(s);
	
	return str;
}

String operator+(String s1, String s2)
{
	char *s = (char*) malloc(sizeof(char) * (s1.size() + s2.size() + 1));
	strcpy(s, s1.data());
	strcat(s, s2.data());
	
	String str(s);
	free(s);
	
	return str;
}

const char* String::data() const
{
	return s;
}

uint16_t String::size()
{
	return length;
}