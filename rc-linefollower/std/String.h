#ifndef STRING_H
#define STRING_H

#include <inttypes.h>
#include <stdlib.h>

class String final
{
public:
	String();
	String(int8_t number);
	String(uint8_t number);
	String(int16_t number);
	String(uint16_t number);	
	
	String(const char* ss);
	String(const String &ss);
	~String();
	
	String& operator=(const String&);	
	
	const char* data() const;
	uint16_t size();
private:
	char *s;
	uint16_t length;
};

String operator+(String &s1, String &s2);
String operator+(String s1, String s2);

#endif /* STRING_H */