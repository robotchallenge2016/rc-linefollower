#include "Setup.h"

#include <avr/io.h>

GPIO GPIO_A(&DDRA, &PORTA, &PINA);
GPIO GPIO_B(&DDRB, &PORTB, &PINB);
GPIO GPIO_C(&DDRC, &PORTC, &PINC);
GPIO GPIO_D(&DDRD, &PORTD, &PIND);

/* UART interface */
Uart Serial1;

/* Periphery */
Battery battery;
BtDriver btDriver;
Engine leftEngine(&GPIO_D, LEFT_ENGINE_IN1, &GPIO_D, LEFT_ENGINE_IN2, &OCR1B);
Engine rightEngine(&GPIO_D, RIGHT_ENGINE_IN1, &GPIO_D, RIGHT_ENGINE_IN2, &OCR1A);
LedDriver ledDriver;
SensorDriver sensorDriver;