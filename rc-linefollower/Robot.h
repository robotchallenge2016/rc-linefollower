#ifndef __ROBOT_H__
#define __ROBOT_H__

#include <inttypes.h>

class Engine;

class Robot
{
private:

public:
	Robot();
	~Robot();
	
	void setup();
	void update();

private:
	void healthCheck();
	void doLinefollower();
	void showSensorStateOnLeds(bool* sensorState);
	void printSensorMeasurements();
	void printSensorState(bool* sensorState);
	void handleBt();
	void handleDirectionCommand(Engine& engine, uint8_t direction);
};

#endif //__ROBOT_H__
