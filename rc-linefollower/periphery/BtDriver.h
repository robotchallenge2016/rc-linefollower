#ifndef __BTDRIVER_H__
#define __BTDRIVER_H__

#include <avr/io.h>

#define BT_DRIVER_KEY_DDR		DDRB
#define BT_DRIVER_KEY_PORT		PORTB
#define BT_DRIVER_KEY_PIN		PB0

#define SLAVE_MODE				0
#define MASTER_MODE				1

class BtDriver
{
private:
	uint32_t baudrate;
	uint8_t *buffer;
	uint8_t buffSize;
	uint8_t mode;
	
public:
	BtDriver();
	~BtDriver();
	
	uint32_t findBaudrate();
	void command(const char* cmd);
	uint8_t available();
	uint8_t read(uint8_t *buffer, uint8_t length);
	void write(const char c);
	void write(const char *s);
	
	uint8_t isMasterMode();
	uint8_t isSlaveMode();	
	void switchToMaster();
	void switchToSlave();
	
	/* Hardware interface */
	void setup();
	void update();
	
protected:
private:
	BtDriver( const BtDriver &c );
	BtDriver& operator=( const BtDriver &c );

	void setKey();
	void clearKey();
};

#endif //__BTDRIVER_H__
