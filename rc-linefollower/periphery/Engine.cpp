#include "Engine.h"
#include "Setup.h"
#include "hardware/GPIO.h"

Engine::Engine(GPIO* inAGpio, uint8_t inAPin, GPIO* inBGpio, uint8_t inBPin, 
				volatile uint16_t* spRegister) 
	: inputAGpio(inAGpio), inputAPin(inAPin), inputBGpio(inBGpio), 
		inputBPin(inBPin), speedRegister(spRegister)
{
	setup();
	stop();
	inputAGpio->setAsOutput( (1 << inputAPin) );
	inputBGpio->setAsOutput( (1 << inputBPin) );
}

void Engine::setup()
{
	TCCR1A |= (1 << COM1A1) | (1 << COM1B1) | (1 << WGM10);
	TCCR1B |= (1 << WGM12) | (1 << CS11) | (1 << CS10);
	DDRD |= (1 << LEFT_ENGINE_PWM) | (1 << RIGHT_ENGINE_PWM);	
}

void Engine::stop()
{
	setSpeed(0);
}

void Engine::setSpeed(uint8_t speed)
{
	currentSpeed = speed;
	*speedRegister = currentSpeed;
}

uint8_t Engine::getSpeed() const
{
	return currentSpeed;
}

void Engine::forward()
{
	inputAGpio->setPins( (1 << inputAPin) );
	inputBGpio->clearPins( (1 << inputBPin) );
}

void Engine::backward()
{
	inputAGpio->clearPins( (1 << inputAPin) );
	inputBGpio->setPins( (1 << inputBPin) );
}
