#include "LedDriver.h"

LedDriver::LedDriver()
{
	LED_DRIVER_DDR |=	(1 << LED_DRIVER_LED0) | (1 << LED_DRIVER_LED1) | 
						(1 << LED_DRIVER_LED2) | (1 << LED_DRIVER_LED3) |
						(1 << LED_DRIVER_LED4) | (1 << LED_DRIVER_LED5) |
						(1 << LED_DRIVER_LED6) | (1 << LED_DRIVER_LED7);
}

LedDriver::~LedDriver()
{
	
}

void LedDriver::turnOnLed(uint8_t led)
{
	LED_DRIVER_PORT |= led;
}

void LedDriver::turnOnAll()
{
	LED_DRIVER_PORT |= 0xFF;
}

void LedDriver::turnOnLed0()
{
	LED_DRIVER_PORT |= (1 << LED_DRIVER_LED0);
}

void LedDriver::turnOnLed1()
{
	LED_DRIVER_PORT |= (1 << LED_DRIVER_LED1);
}

void LedDriver::turnOnLed2()
{
	LED_DRIVER_PORT |= (1 << LED_DRIVER_LED2);
}

void LedDriver::turnOnLed3()
{
	LED_DRIVER_PORT |= (1 << LED_DRIVER_LED3);
}

void LedDriver::turnOnLed4()
{
	LED_DRIVER_PORT |= (1 << LED_DRIVER_LED4);
}

void LedDriver::turnOnLed5()
{
	LED_DRIVER_PORT |= (1 << LED_DRIVER_LED5);
}

void LedDriver::turnOnLed6()
{
	LED_DRIVER_PORT |= (1 << LED_DRIVER_LED6);
}

void LedDriver::turnOnLed7()
{
	LED_DRIVER_PORT |= (1 << LED_DRIVER_LED7);
}

void LedDriver::turnOffLed(uint8_t led)
{
	LED_DRIVER_PORT &= ~led;
}

void LedDriver::turnOffAll()
{
	LED_DRIVER_PORT &= 0x00;
}

void LedDriver::turnOffLed0()
{
	LED_DRIVER_PORT &= ~(1 << LED_DRIVER_LED0);
}

void LedDriver::turnOffLed1()
{
	LED_DRIVER_PORT &= ~(1 << LED_DRIVER_LED1);
}

void LedDriver::turnOffLed2()
{
	LED_DRIVER_PORT &= ~(1 << LED_DRIVER_LED2);
}

void LedDriver::turnOffLed3()
{
	LED_DRIVER_PORT &= ~(1 << LED_DRIVER_LED3);
}

void LedDriver::turnOffLed4()
{
	LED_DRIVER_PORT &= ~(1 << LED_DRIVER_LED4);
}

void LedDriver::turnOffLed5()
{
	LED_DRIVER_PORT &= ~(1 << LED_DRIVER_LED5);
}

void LedDriver::turnOffLed6()
{
	LED_DRIVER_PORT &= ~(1 << LED_DRIVER_LED6);
}

void LedDriver::turnOffLed7()
{
	LED_DRIVER_PORT &= ~(1 << LED_DRIVER_LED7);
}
