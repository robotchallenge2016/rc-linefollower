#ifndef __SENSORDRIVER_H__
#define __SENSORDRIVER_H__

#include <inttypes.h>

class SensorDriver
{
private:
	uint8_t sensorCount;
	uint8_t currentChannel;
	uint16_t *results;

public:
	SensorDriver();
	~SensorDriver();
	
	void resultReady();
	
	uint16_t sensor0();
	uint16_t sensor1();
	uint16_t sensor2();
	uint16_t sensor3();
	uint16_t sensor4();
	uint16_t sensor5();
	uint16_t sensor6();
	uint16_t sensor7();
private:
	void changeChannel(uint8_t channel);
	void startMeasurement();
};

#endif //__SENSORDRIVER_H__
