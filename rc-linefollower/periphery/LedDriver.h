#ifndef __LEDDRIVER_H__
#define __LEDDRIVER_H__

#include <avr/io.h>
#include <inttypes.h>

#define LED_DRIVER_DDR		DDRC
#define LED_DRIVER_PORT		PORTC

#define LED_DRIVER_LED0		PC0
#define LED_DRIVER_LED1		PC1
#define LED_DRIVER_LED2		PC2
#define LED_DRIVER_LED3		PC3
#define LED_DRIVER_LED4		PC4
#define LED_DRIVER_LED5		PC5
#define LED_DRIVER_LED6		PC6
#define LED_DRIVER_LED7		PC7

class LedDriver
{
private:

public:
	LedDriver();
	~LedDriver();
	
	void turnOnLed(uint8_t led);
	void turnOnAll();
	void turnOnLed0();
	void turnOnLed1();
	void turnOnLed2();
	void turnOnLed3();
	void turnOnLed4();
	void turnOnLed5();
	void turnOnLed6();
	void turnOnLed7();
	
	void turnOffLed(uint8_t led);
	void turnOffAll();
	void turnOffLed0();
	void turnOffLed1();
	void turnOffLed2();
	void turnOffLed3();
	void turnOffLed4();
	void turnOffLed5();
	void turnOffLed6();
	void turnOffLed7();	
private:

};

#endif //__LEDDRIVER_H__
