#ifndef ENGINE_H
#define ENGINE_H

#include <stdint.h>

#define LEFT_ENGINE_IN1				PD3
#define LEFT_ENGINE_IN2				PD2
#define RIGHT_ENGINE_IN1			PD6
#define RIGHT_ENGINE_IN2			PD7

#define LEFT_ENGINE_PWM				PD4
#define RIGHT_ENGINE_PWM			PD5

class GPIO;

class Engine
{
	public:
	Engine(GPIO* dirGpio, uint8_t dirPin, GPIO* spGpio, uint8_t spPin,
			volatile uint16_t* spRegister);
	
	void setup();
	void stop();
	void setSpeed(uint8_t speed);
	uint8_t getSpeed() const;
	void forward();
	void backward();
	
	private:
	GPIO* inputAGpio;
	uint8_t inputAPin;
	GPIO* inputBGpio;
	uint8_t inputBPin;
	volatile uint16_t* speedRegister;
	uint8_t currentSpeed;
};

#endif //ENGINE_H