#include "SensorDriver.h"

#include "Setup.h"

#include <avr/io.h>
#include <avr/interrupt.h>

SensorDriver::SensorDriver() : sensorCount(8), currentChannel(0)
{
	results = new uint16_t[sensorCount];
	
	// AVCC as a voltage reference 
	ADMUX |= (1 << REFS0);
	ADCSRA |= (1 << ADEN) | (1 << ADIE);
	// F_ADC = F_CPU / 8
	ADCSRA |= (1 << ADPS1) | (1 << ADPS0);
	
	startMeasurement();
}

SensorDriver::~SensorDriver()
{
	
}

void SensorDriver::resultReady()
{
	results[currentChannel] = ADC;
	++currentChannel;
	currentChannel %= sensorCount;
	changeChannel(currentChannel);
	startMeasurement();
}

void SensorDriver::changeChannel(uint8_t channel)
{
	ADMUX &= 0xE0;
	ADMUX |= channel;
}

void SensorDriver::startMeasurement()
{
	ADCSRA |= (1 << ADSC);
}

uint16_t SensorDriver::sensor0()
{
	return results[4];
}

uint16_t SensorDriver::sensor1()
{
	return results[5];
}

uint16_t SensorDriver::sensor2()
{
	return results[6];
}

uint16_t SensorDriver::sensor3()
{
	return results[7];
}

uint16_t SensorDriver::sensor4()
{
	return results[0];
}

uint16_t SensorDriver::sensor5()
{
	return results[1];
}

uint16_t SensorDriver::sensor6()
{
	return results[2];
}

uint16_t SensorDriver::sensor7()
{
	return results[3];
}

ISR(ADC_vect)
{
	sensorDriver.resultReady();
}
