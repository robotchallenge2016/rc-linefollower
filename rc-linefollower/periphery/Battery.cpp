#include "Battery.h"

#include "time/delays.h"
#include "Setup.h"

#include <avr/io.h>

Battery::Battery()
{
	gpio = &GPIO_B;
	gpio->setAsInput( (1 << BATTERY_SENSE_PIN_7V2) );
	gpio->setAsInput( (1 << BATTERY_SENSE_PIN_7V6) );
	gpio->setAsOutput( (1 << BATTERY_LED_PIN) );
	
	gpio->setPins( (1 << BATTERY_SENSE_PIN_7V2) | (1 << BATTERY_SENSE_PIN_7V6) );
	
	turnOffLed();
}

Battery::~Battery()
{
	
}

void Battery::turnOnLed()
{
	gpio->setPins( (1 << BATTERY_LED_PIN) );
}

void Battery::turnOffLed()
{
	gpio->clearPins( (1 << BATTERY_LED_PIN) );
}

void Battery::blinkLed()
{
	turnOnLed();
	delay100ms();
	turnOffLed();
	delay100ms();
}

uint8_t Battery::status()
{
	if(gpio->readPin(BATTERY_SENSE_PIN_7V2))
	{
		return BATTERY_LOW;
	}
	return BATTERY_OK;
}
