#include "BtDriver.h"
#include "Setup.h"
#include "time/delays.h"

#include <stdio.h>
#include <stdlib.h>

BtDriver::BtDriver() : mode(SLAVE_MODE)
{
	baudrate = 0;
	buffSize = 32;
	buffer = (uint8_t*) malloc(sizeof(uint8_t) * buffSize);
}

BtDriver::~BtDriver()
{
	free(buffer);
}

uint32_t BtDriver::findBaudrate()
{
	const uint32_t rates[] = { 4800, 9600, 19200, 38400, 57600, 115200 };
	uint8_t numRates = sizeof(rates) / sizeof(uint32_t);
	
	setKey();	
	for(uint8_t j = 0; j < numRates; ++j)
	{
		Serial1.begin(rates[j]);
		Serial1.setTimeout(1000);
		
		Serial1.print("AT\r\n");
		
		uint8_t n = Serial1.readBytesUntil('\n', buffer, buffSize);
		if(n > 0)
		{
			baudrate = rates[j];
		}
	}
	clearKey();
	return baudrate;	
}

void BtDriver::command(const char* cmd)
{
	if(isSlaveMode())
	{
		setKey();	
	}
	Serial1.print(cmd);
	Serial1.print("\r\n");
	
	uint8_t n;
	do 
	{
		n = Serial1.readBytesUntil('\n', buffer, buffSize-1);
		buffer[n] = 0;
	} while(n > 0 && (buffer[0] != 'O') && (buffer[1] != 'K'));
	
	if(isSlaveMode())
	{
		clearKey();	
	}
}

uint8_t BtDriver::available()
{
	return Serial1.available();
}

uint8_t BtDriver::read(uint8_t *buffer, uint8_t length)
{
	return Serial1.readBytes(buffer, length);
}

void BtDriver::write(const char c)
{
	Serial1.write(c);
}

void BtDriver::write(const char *s)
{
	Serial1.print(s);
}

void BtDriver::setKey()
{
	BT_DRIVER_KEY_PORT |= (1 << BT_DRIVER_KEY_PIN);
	delay100ms();
}

void BtDriver::clearKey()
{
	BT_DRIVER_KEY_PORT &= ~(1 << BT_DRIVER_KEY_PIN);
	delay200ms();
}

uint8_t BtDriver::isMasterMode()
{
	return (mode == MASTER_MODE);
}

uint8_t BtDriver::isSlaveMode()
{
	return (mode == SLAVE_MODE);
}

void BtDriver::switchToMaster()
{
	setKey();
	mode = MASTER_MODE;
}

void BtDriver::switchToSlave()
{
	clearKey();
	mode = SLAVE_MODE;
}

/* Hardware interface */
void BtDriver::setup()
{
	BT_DRIVER_KEY_DDR |= (1 << BT_DRIVER_KEY_PIN);
}

void BtDriver::update()
{
	
}