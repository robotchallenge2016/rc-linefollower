#ifndef __BATTERY_H__
#define __BATTERY_H__

#define BATTERY_OK					1
#define BATTERY_LOW					0
#define BATTERY_SENSE_PIN_7V2		PB1
#define BATTERY_SENSE_PIN_7V6		PB0
#define BATTERY_LED_PIN				PB2

#include <inttypes.h>

class GPIO;

class Battery
{
private:

//functions
public:
	Battery();
	~Battery();
	
	void turnOnLed();
	void turnOffLed();
	void blinkLed();
	uint8_t status();
private:
	GPIO* gpio;
};

#endif //__BATTERY_H__
