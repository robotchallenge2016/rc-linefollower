#ifndef __TIMEOUT_H__
#define __TIMEOUT_H__

#define DEFAULT_TIMEOUT		1000

class Timeout
{
public:
	virtual ~Timeout(){}
	virtual void timeUp() = 0;
	virtual void setTimeout(uint32_t timeout) = 0;

};

#endif //__TIMEOUT_H__
